#XML Editor for "Red Faction Guerrilla" game

This is a fast XML assets editor for "Red Faction Guerrilla" game

![screen](rfg-editor.jpeg)

####Usage:
* open `index.html` in browser
* Hit "Choose file" button, choose `*.xtbl` file from game installation
* Put checkboxes in middle section for items you want to modify
* You can edit entities in middle section, click on title to show content
* Right section is meant for batch updates - it's values are multipliers
* Copy content from text are in top left corner - this is a content fro new `.xtbl` file (note that font size there is very small - you won't see what you copy - just hit `cmd+a` and then `cmd+c` )
* Refer to this article if you don't know what to do with new file: https://steamcommunity.com/app/667720/discussions/0/1726450077681654883/
